#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2014 Roger Light <roger@atchoo.org>
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Distribution License v1.0
# which accompanies this distribution.
#
# The Eclipse Distribution License is available at
#   http://www.eclipse.org/org/documents/edl-v10.php.
#
# Contributors:
#    Roger Light - initial implementation

# This shows an example of using the publish.single helper function.

import paho.mqtt.publish as publish
import time
import os 

SERVER = os.environ['SERVER']
PORT = os.environ['PORT']
ITERATIONS = int(os.environ['ITERATIONS'])
SLEEP = int(os.environ['INTERVAL'])

for i in range(0, ITERATIONS):
    msg = "Dummy-MESSAGE: %s" % i
    publish.single("$UB/dummy/single", payload=msg, hostname=SERVER, port=PORT)
    print("--->PUBLISHING to DUMMY: %s" % msg)
    time.sleep(SLEEP)
