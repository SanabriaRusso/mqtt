# How to launch the MQTT environment using Docker Compose?
In this directory you will find the Python code for the **sub** and **pub** clients for subscribing, and publishing to a topic, respectively.

Also within those directories, a DockerFile can be found. This DockerFile tells Docker how to build the corresponding images so later Docker containers can be launched.

The main file used for deploying the example scenario is the so-called `mqtt/mqtt_service.yml`.

To launch the example environment and peeking at the generated logs, issue the following command:

```bash
docker-compose -f mqtt_service.yml up --build
```

If you want to run the environment as a daemon, append the `-d` flag.

Figure below shows the topology of the aforementioned environment.

![Docker-Compose Example MQTT Environment Topology][topo]

## Looking at the output of a Docker container (logs)
Each container runs with unbuffered logs/output. That is, you may simply issue:
```bash
docker logs -f <containerID>
```
, replacing `containerID` with the id of the appropriate container (you can get it via `docker ps -a`).

[topo]: figures/topo.png "Docker-Compose Example MQTT Environment Topology"